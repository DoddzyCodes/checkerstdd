#include "CheckersGame.h"
#include "CheckersPieces.h"
namespace Checkers {
Game::Game(std::unique_ptr<Board> board)
    : m_board(std::move(board)), m_currentPlayer(Game::Players::White)
{
}

bool Game::MakeMove(Game::Players player,
                    Board::BoardPos start,
                    Board::BoardPos end)
{
    if (player != m_currentPlayer)
    {
        return false;
    }

    Piece startPiece = m_board->pieceAt(start);
    Piece endPiece   = m_board->pieceAt(end);

    Piece piece =
        (player == Game::Players::White) ? Piece::White : Piece::Black;

    if (startPiece == Piece::Invalid || startPiece != piece)
    {
        return false;
    }
    if (endPiece != Piece::Empty)
    {
        return false;
    }

    int horzDistance = end.x - start.x;
    int vertDistance = end.y - start.y;

    if ((abs(horzDistance) != 1) ||
        (startPiece == Piece::White && (vertDistance != 1)) ||
        (startPiece == Piece::Black && (vertDistance != -1)))
    {
        return false;
    }

    m_board->placeAt(start, Piece::Empty);
    m_board->placeAt(end, piece);

    m_currentPlayer = (m_currentPlayer == Game::Players::White)
                          ? Game::Players::Black
                          : Game::Players::White;

    return true;
}
} // Namespace Checkers