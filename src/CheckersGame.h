#pragma once

#include "CheckersBoard.h"
#include "CheckersPieces.h"

#include <memory>

namespace Checkers {
class Game
{
  public:
    enum class Players
    {
        White,
        Black
    };

  public:
    Game(const std::unique_ptr<Board> board);

    Board& getBoard() const
    {
        return *(m_board.get());
    }
    Players getCurrentPlayer() const
    {
        return m_currentPlayer;
    }

    bool MakeMove(Players player, Board::BoardPos start, Board::BoardPos end);

  private:
    std::unique_ptr<Board> m_board;

    Players m_currentPlayer;
};
}