
#include "catch.hpp"

#include "CheckersGame.h"

TEST_CASE("Testing board setup", "[BOARD]")
{

    SECTION("Create Empty Board")
    {

        Checkers::Board b;

        REQUIRE(b.pieceAt({0, 0}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({1, 0}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 0}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({3, 0}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 0}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({5, 0}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 0}) == Checkers::Piece::Empty);

        REQUIRE(b.pieceAt({7, 0}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 1}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({2, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 1}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({4, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 1}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({6, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 1}) == Checkers::Piece::Empty);

        REQUIRE(b.pieceAt({0, 2}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({1, 2}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 2}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({3, 2}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 2}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({5, 2}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 2}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({7, 2}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 3}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({2, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 3}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({4, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 3}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({6, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 3}) == Checkers::Piece::Empty);

        REQUIRE(b.pieceAt({0, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({1, 4}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({3, 4}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({5, 4}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({7, 4}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 5}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({2, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 5}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({4, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 5}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({6, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 5}) == Checkers::Piece::Empty);

        REQUIRE(b.pieceAt({0, 6}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({1, 6}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 6}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({3, 6}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 6}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({5, 6}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 6}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({7, 6}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 7}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({2, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 7}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({4, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 7}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({6, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 7}) == Checkers::Piece::Empty);
    }

    SECTION("Create Default Board")
    {

        Checkers::Board b(Checkers::Board::BoardType::Default);

        REQUIRE(b.pieceAt({0, 0}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({1, 0}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 0}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({3, 0}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 0}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({5, 0}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 0}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({7, 0}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 1}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({2, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 1}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({4, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 1}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({6, 1}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 1}) == Checkers::Piece::White);

        REQUIRE(b.pieceAt({0, 2}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({1, 2}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 2}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({3, 2}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 2}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({5, 2}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 2}) == Checkers::Piece::White);
        REQUIRE(b.pieceAt({7, 2}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 3}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({2, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 3}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({4, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 3}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({6, 3}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 3}) == Checkers::Piece::Empty);

        REQUIRE(b.pieceAt({0, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({1, 4}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({3, 4}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({5, 4}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 4}) == Checkers::Piece::Empty);
        REQUIRE(b.pieceAt({7, 4}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 5}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({2, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 5}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({4, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 5}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({6, 5}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 5}) == Checkers::Piece::Black);

        REQUIRE(b.pieceAt({0, 6}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({1, 6}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({2, 6}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({3, 6}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({4, 6}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({5, 6}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({6, 6}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({7, 6}) == Checkers::Piece::Invalid);

        REQUIRE(b.pieceAt({0, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({1, 7}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({2, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({3, 7}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({4, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({5, 7}) == Checkers::Piece::Black);
        REQUIRE(b.pieceAt({6, 7}) == Checkers::Piece::Invalid);
        REQUIRE(b.pieceAt({7, 7}) == Checkers::Piece::Black);
    }

    SECTION("Create custom board")
    {
        Checkers::Board b; // Empty board

        // Check placing on valid square
        REQUIRE(b.pieceAt({0, 0}) == Checkers::Piece::Empty);
        b.placeAt({0, 0}, Checkers::Piece::White);
        REQUIRE(b.pieceAt({0, 0}) == Checkers::Piece::White);

        // Check failed placing on invalid square
        REQUIRE(b.pieceAt({1, 0}) == Checkers::Piece::Invalid);
        b.placeAt({1, 0}, Checkers::Piece::White);
        REQUIRE(b.pieceAt({1, 0}) == Checkers::Piece::Invalid);
    }
}