#include "catch.hpp"
#include <memory>

#include "CheckersGame.h"
#include "CheckersPieces.h"

TEST_CASE("Testing Game Code", "[GAME]")
{

    SECTION("Setup New Game")
    {
        auto board = std::make_unique<Checkers::Board>(
            Checkers::Board::BoardType::Default);
        Checkers::Game game(std::move(board));

        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
    }

    SECTION("Make a valid move")
    {
        auto board = std::make_unique<Checkers::Board>(
            Checkers::Board::BoardType::Default);
        Checkers::Game game(std::move(board));

        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({0, 2}) == Checkers::Piece::White);
        REQUIRE(game.getBoard().pieceAt({1, 3}) == Checkers::Piece::Empty);

        auto madeMove =
            game.MakeMove(Checkers::Game::Players::White, {0, 2}, {1, 3});

        REQUIRE(madeMove);
        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::Black);
        REQUIRE(game.getBoard().pieceAt({0, 2}) == Checkers::Piece::Empty);
        REQUIRE(game.getBoard().pieceAt({1, 3}) == Checkers::Piece::White);
    }

    SECTION("Make an invalid move -  Wrong player")
    {
        auto board = std::make_unique<Checkers::Board>(
            Checkers::Board::BoardType::Default);
        Checkers::Game game(std::move(board));

        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({1, 5}) == Checkers::Piece::Black);
        REQUIRE(game.getBoard().pieceAt({0, 4}) == Checkers::Piece::Empty);
        auto madeMove =
            game.MakeMove(Checkers::Game::Players::Black, {1, 5}, {0, 4});

        REQUIRE(!madeMove);
        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({1, 5}) == Checkers::Piece::Black);
        REQUIRE(game.getBoard().pieceAt({0, 4}) == Checkers::Piece::Empty);
    }

    SECTION("Make an invalid move - Invalid start/end")
    {
        auto board = std::make_unique<Checkers::Board>(
            Checkers::Board::BoardType::Default);
        Checkers::Game game(std::move(board));

        auto madeMove =
            game.MakeMove(Checkers::Game::Players::White, {0, 2}, {2, 4});
        REQUIRE(!madeMove);

        madeMove =
            game.MakeMove(Checkers::Game::Players::White, {0, 2}, {0, 3});
        REQUIRE(!madeMove);

        madeMove =
            game.MakeMove(Checkers::Game::Players::White, {0, 3}, {1, 3});
        REQUIRE(!madeMove);
    }

    SECTION("Make an invalid move - Move onto existing piece")
    {

        auto board = std::make_unique<Checkers::Board>(
            Checkers::Board::BoardType::Default);
        Checkers::Game game(std::move(board));

        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({0, 0}) == Checkers::Piece::White);
        REQUIRE(game.getBoard().pieceAt({1, 1}) == Checkers::Piece::White);
        auto madeMove =
            game.MakeMove(Checkers::Game::Players::White, {0, 0}, {1, 1});

        REQUIRE(!madeMove);
        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({0, 0}) == Checkers::Piece::White);
        REQUIRE(game.getBoard().pieceAt({1, 1}) == Checkers::Piece::White);
    }

    SECTION("Make a valid jump")
    {
        auto board = std::make_unique<Checkers::Board>(
            Checkers::Board::BoardType::Empty);

        board->placeAt({0, 0}, Checkers::Piece::White);
        board->placeAt({1, 1}, Checkers::Piece::Black);

        Checkers::Game game(std::move(board));

        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({0, 0}) == Checkers::Piece::White);
        REQUIRE(game.getBoard().pieceAt({1, 1}) == Checkers::Piece::Black);
        auto madeMove =
            game.MakeMove(Checkers::Game::Players::White, {0, 0}, {2, 2});

        REQUIRE(madeMove);
        REQUIRE(game.getCurrentPlayer() == Checkers::Game::Players::White);
        REQUIRE(game.getBoard().pieceAt({2, 2}) == Checkers::Piece::White);
        REQUIRE(game.getBoard().pieceAt({1, 1}) == Checkers::Piece::Empty);
    }
}