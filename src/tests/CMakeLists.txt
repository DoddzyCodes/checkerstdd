cmake_minimum_required(VERSION 3.0.0)
project(checkers_tests VERSION 0.0.0)

include_directories(${CMAKE_SOURCE_DIR}/deps/catch)

set(SRC
    tests_main.cpp
    tests_board.cpp
    tests_game.cpp
)


include_directories("..")
add_executable(${PROJECT_NAME} ${SRC})
target_link_libraries(${PROJECT_NAME} checkers)

# configure unit tests via CTest
enable_testing()
add_test(NAME RunTests COMMAND ${PROJECT_NAME})
#catch_discover_tests(${PROJECT_NAME})

run_clang_tidy(${PROJECT_NAME})