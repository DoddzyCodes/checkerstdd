#define CATCH_CONFIG_MAIN

#include "catch.hpp"

#include <vector>
TEST_CASE("Test Suite Testing", "[TEST]")
{
    std::vector<int> ints(5);

    REQUIRE(ints.size() == 5);
    REQUIRE(ints.capacity() >= 5);
}