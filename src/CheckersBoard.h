#include "CheckersPieces.h"
#include <bitset>
#include <stdint.h>

namespace Checkers {
class Board
{
    using BoardIndex = uint_fast8_t;

  public:
    enum class BoardType
    {
        Empty,
        Default
    };

    struct BoardPos
    {
        BoardIndex x;
        BoardIndex y;
        BoardIndex toIndex() const
        {
            // Assumes 8x8 board!
            return (y * 8) + x;
            ;
        }
    };

  public:
    Board(BoardType type = BoardType::Empty);

    Piece pieceAt(const BoardPos& pos) const;
    void placeAt(const BoardPos& pos, const Piece& piece);

  private:
    using PieceBitField = std::bitset<32>;

    PieceBitField m_whitePieces;
    PieceBitField m_blackPieces;
};
}