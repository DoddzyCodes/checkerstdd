#include "CheckersBoard.h"
#include <cassert>
#include <cstring>
#include <iostream>

namespace Checkers {

Board::Board(BoardType type)
{
    if (type == BoardType::Default)
    {
        m_whitePieces = 0x00000FFF;
        m_blackPieces = 0xFFF00000;
    }
}

Piece Board::pieceAt(const BoardPos& pos) const
{
    // std::cout << m_whitePieces.to_string() << std::endl;
    // std::cout << m_blackPieces.to_string() << std::endl;

    BoardIndex idx = pos.toIndex();
    // valid board positions are dependant on the row
    // for 'even' y index, x positions must be even
    // for 'odd' y index, x positions must be odd
    Piece piece = Piece::Invalid;
    if ((pos.y & 1) == (pos.x & 1))
    {
        BoardIndex bitIndex = idx / 2;
        if (m_whitePieces[bitIndex])
        {
            piece = Piece::White;
        }
        else if (m_blackPieces[bitIndex])
        {
            piece = Piece::Black;
        }
        else
        {
            piece = Piece::Empty;
        }
    }

    return piece;
}

void Board::placeAt(const BoardPos& pos, const Piece& piece)
{
    if (pieceAt(pos) != Piece::Invalid)
    {
        BoardIndex bitIndex = pos.toIndex() / 2;
        if (piece == Piece::Empty)
        {
            m_whitePieces[bitIndex] = false;
            m_blackPieces[bitIndex] = false;
        }
        else if (piece == Piece::White)
        {
            m_whitePieces[bitIndex] = true;
            m_blackPieces[bitIndex] = false;
        }
        else if (piece == Piece::Black)
        {
            m_whitePieces[bitIndex] = false;
            m_blackPieces[bitIndex] = true;
        }
    }
}
} // namespace Checkers