#pragma once

namespace Checkers {
enum class Piece
{
    Empty,
    White,
    Black,
    Invalid
};
}